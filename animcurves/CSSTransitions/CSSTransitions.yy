{
  "function": 2,
  "channels": [
    {"colour":4283622883,"visible":true,"points":[
        {"th0":-0.25,"th1":0.245958686,"tv0":0.0,"tv1":1.55103278,"x":0.0,"y":0.0,},
        {"th0":-0.1607408,"th1":0.25,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.0,},
      ],"resourceVersion":"1.0","name":"ease-back","tags":[],"resourceType":"GMAnimCurveChannel",},
    {"colour":4281083598,"visible":true,"points":[
        {"th0":-0.25,"th1":0.0,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":-0.2544392,"th1":0.141653627,"tv0":-0.121180363,"tv1":0.06746459,"x":0.3424808,"y":0.9329951,},
        {"th0":-0.2,"th1":0.25,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.0,},
      ],"resourceVersion":"1.0","name":"expo","tags":[],"resourceType":"GMAnimCurveChannel",},
  ],
  "parent": {
    "name": "Animation Curves",
    "path": "folders/Animation Curves.yy",
  },
  "resourceVersion": "1.2",
  "name": "CSSTransitions",
  "tags": [],
  "resourceType": "GMAnimCurve",
}