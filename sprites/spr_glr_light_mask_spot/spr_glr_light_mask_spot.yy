{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 255,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"789e0cea-7b3f-44be-8688-b2212c122137","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"789e0cea-7b3f-44be-8688-b2212c122137","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},"LayerId":{"name":"c44cd252-d0d9-40ee-9679-ce905ed3147e","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_glr_light_mask_spot","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},"resourceVersion":"1.0","name":"789e0cea-7b3f-44be-8688-b2212c122137","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_glr_light_mask_spot","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"8af8c173-7bee-4adf-b93d-5b6bb793d95f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"789e0cea-7b3f-44be-8688-b2212c122137","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_glr_light_mask_spot","path":"sprites/spr_glr_light_mask_spot/spr_glr_light_mask_spot.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c44cd252-d0d9-40ee-9679-ce905ed3147e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Masks",
    "path": "folders/Bulb/Masks.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_glr_light_mask_spot",
  "tags": [],
  "resourceType": "GMSprite",
}