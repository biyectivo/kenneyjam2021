/// @function fnc_SetGraphics
/// @description Update graphics

function fnc_SetGraphics() {

	if (argument_count>1) {
		var _RESOLUTION_W = argument[0];
		var _RESOLUTION_H = argument[1];
	}
	else {
		var _RESOLUTION_W = BASE_RESOLUTION_W;
		var _RESOLUTION_H = BASE_RESOLUTION_H;
	}
	
	

	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW) { // Adjust desired Game resolution (camera) to match display size/aspect ratio and then set window equal to game resolution
		
		// Adjust resolution with the selected aspect ratio
		
		if (SELECTED_ASPECT_RATIO<1) { // Portrait aspect ratio
			adjusted_resolution_width = min(_RESOLUTION_W, DISPLAY_WIDTH);
			adjusted_resolution_height = floor(adjusted_resolution_width / SELECTED_ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_resolution_height = min(_RESOLUTION_H, DISPLAY_HEIGHT);
			adjusted_resolution_width = floor(adjusted_resolution_height * SELECTED_ASPECT_RATIO);
		}
		
		// Keep window size except fullscreen (in which case window size = display size)
		
		if (window_get_fullscreen()) {
			adjusted_window_width = DISPLAY_WIDTH;
			adjusted_window_height = DISPLAY_HEIGHT;			
		}
		else {			
			adjusted_window_width = BASE_WINDOW_SIZE_W;
			adjusted_window_height = BASE_WINDOW_SIZE_H;
		}
	}
	else if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW || SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) { // Leave Game resolution (camera) fixed (as long as <= window) and adjust desired window resolution to match display size/aspect ratio
		
		// Adjust window size with the selected aspect ratio
		
		if (SELECTED_ASPECT_RATIO < 1) { // Portrait aspect ratio
			adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
			adjusted_window_height = floor(adjusted_window_width / SELECTED_ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
			adjusted_window_width = floor(adjusted_window_height * SELECTED_ASPECT_RATIO);
		}
	
		if (window_get_fullscreen()) {
			adjusted_window_width = DISPLAY_WIDTH;
			adjusted_window_height = DISPLAY_HEIGHT;			
		}
		else {			
			adjusted_window_width = BASE_WINDOW_SIZE_W;
			adjusted_window_height = BASE_WINDOW_SIZE_H;
		}
		
		// Clamp resolution size to the window size
		
		adjusted_resolution_width = min(_RESOLUTION_W, adjusted_window_width);
		adjusted_resolution_height = min(_RESOLUTION_H, adjusted_window_height);		
		
	}
	
	// Redefine camera size
	camera_set_view_pos(VIEW, 0, 0);
	camera_set_view_size(VIEW, adjusted_resolution_width, adjusted_resolution_height);
	
	// Resize the application surface to the desired game resolution width/height
	surface_resize(application_surface, adjusted_resolution_width, adjusted_resolution_height);
	
	// Set window size
	window_set_size(adjusted_window_width, adjusted_window_height);
	
	// Browser maximize hack	
	if (BROWSER && option_value[? "Fullscreen"]) {
		window_set_size(browser_height*SELECTED_ASPECT_RATIO, browser_height);
	}
	
	// Center the window (on the next few step)
	if (CENTER_WINDOW) {
		alarm[0] = 2;
	}
	
}
