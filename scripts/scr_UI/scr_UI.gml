
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _y_title = 60;
		
		type_formatted(_w/2, _y_title, "[fnt_Title][fa_middle][fa_center][scale,1.2]"+_title_color+game_title);
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 100;
		var _spacing = 40;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center]"+_link_color+menu_items[_i], "[fa_middle][fa_center]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
		
		type_formatted(_w/2, _h-100, "[fnt_Title][fa_middle][fa_center][scale,0.8]A game by José Bonilla for [spr_LudumDare] 48");
			
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;

		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);	
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 60;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-30, _link_color+"[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	
	if (ENABLE_LIVE && live_call()) return live_result;
	audio_stop_all();
	
	if (Game.game_over_played) {
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_GameOver, 1, false);
			Game.game_over_played = true;
		}
	}
	fnc_BackupDrawParams();
	// Draw the HUD
	
	draw_set_color(c_black);
	draw_set_alpha(0.8);
	draw_rectangle(0, 0, GUI_WIDTH, GUI_HEIGHT, false);
	var _format1 = "[fnt_Title][c_white][fa_center][fa_middle]";
	var _format2 = "[fnt_GUI][c_white][fa_center][fa_middle]";
	var _format3 = "[fnt_GUI][c_gray][fa_center][fa_middle]";
	var _format4 = "[fnt_GUI][c_lime][fa_center][fa_middle]";
	
	
	type_formatted(GUI_WIDTH/2, 120, _format2+"Total points: ");
	type_formatted(GUI_WIDTH/2, 200, _format1+string(Game.total_score));
	type_formatted(GUI_WIDTH/2, 280, _format2+"Max Level: "+_format3+string(Game.level+1));
	
	if (Game.total_score > Game.high_score) {
		var _fid = file_text_open_write("high_score.dat");
		file_text_write_string(_fid, string(Game.total_score)+" "+string(Game.level+1)+" "+sha1_string_utf8(string(Game.total_score)+" "+string(Game.level+1)+" "+SCOREBOARD_SALT));
		file_text_close(_fid);
		show_debug_message("High score registered");
		Game.high_score = Game.total_score;
		Game.high_level = Game.level+1;
		type_formatted(GUI_WIDTH/2, 400, _format4+"NEW HIGH SCORE!");
	}
	else {
		type_formatted(GUI_WIDTH/2, 400, _format2+"YOUR HIGH SCORE: "+_format3+string(Game.high_score));
	}
	
	if (keyboard_check_pressed(vk_enter)  && !keyboard_check(vk_lalt)) {
		game_restart();
	}
	
	
	fnc_RestoreDrawParams();
}

function fnc_DrawHUD() {
	if (ENABLE_LIVE && live_call()) return live_result;
	fnc_BackupDrawParams();
	// Draw the HUD
	
		draw_set_color(c_black);
		draw_set_alpha(0.8);
		draw_rectangle(0, 0, room_width, 80, false);
		var _format1 = "[fnt_GUI][c_white][fa_center][fa_middle]";
		var _format2 = "[fnt_GUI][c_gray][fa_center][fa_middle]";
		var _format3 = "[fnt_GUI][$333333][fa_center][fa_middle]";
		var _n = obj_LevelManager.board_columns*obj_LevelManager.board_rows;
		var _l = fnc_AnimalsLeft();
		type_formatted(room_width/2, 40,	_format1+"Points: "+_format2+string(Game.total_score)+
								_format3+"  "+
								_format1+"Level: "+_format2+string(Game.level+1)+
								_format3+"  "+
								_format1+"[scale,"+string(0.2)+"][spr_Chick][scale,1]: "+_format2+string(_n-_l)+_format1+"/"+_format2+string(_n)+
								_format3+"  "+
								_format1+"Strikes: "+_format2+string(Game.strikes)+_format1+"/"+_format2+"3"
								);
	
	fnc_RestoreDrawParams();
}

#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.7);
		draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(20, 20,	"DEBUG MODE");
		
		var _spacing = 50;
		
		var _debug_data = [];
		
		_debug_data[0] = "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")";
		_debug_data[1] = "Requested scaling type: "+string(SELECTED_SCALING)+" ("+
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW ? "Normal mode (resolution scaled to window)" : 
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW ? "Resolution independent of window, unscaled" : "Resolution independent of of window, scaled")+")");
		_debug_data[2] = "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H);
		_debug_data[3] = "Actual base resolution: "+string(adjusted_resolution_width)+"x"+string(adjusted_resolution_height);
		_debug_data[4] = "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H);
		_debug_data[5] = "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height);
		_debug_data[6] = "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")";
		_debug_data[7] = "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")";
		_debug_data[8] = "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")";
		_debug_data[9] = "GUI Layer (with helper functions!): "+string(GUI_WIDTH)+"x"+string(GUI_HEIGHT)+" ("+string(round(GUI_WIDTH/GUI_HEIGHT * 100)/100)+")";
		_debug_data[10] = "Mouse: "+string(mouse_x)+","+string(mouse_y);
		_debug_data[11] = "Device mouse 0: "+string(device_mouse_x(0))+","+string(device_mouse_y(0));
		_debug_data[12] = "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0));		
		_debug_data[13] = "Paused: "+string(paused);
		_debug_data[14] = "FPS: "+string(fps_real) + "/" + string(fps);
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(20, _spacing+_i*15, _debug_data[_i]);
			show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _mousex = device_mouse_x_to_gui(0);
	var _mousey = device_mouse_y_to_gui(0);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = _mousex >= _bbox_coords[0] && _mousey >= _bbox_coords[1] && _mousex <= _bbox_coords[2] && _mousey <= _bbox_coords[3];
	//var _mouseover = _mousex >= _draw_data_normal.bbox_x1 && _mousey >= _draw_data_normal.bbox_y1 && _mousex <= _draw_data_normal.bbox_x2 && _mousey <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			script_execute(_callback, _param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	script_execute(asset_get_index("fnc_Menu_"+string(_param)));
}

function fnc_ExecuteOptions(_param) {
	script_execute(asset_get_index("fnc_Options_"+string(_param)));
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
	
function fnc_AnimalsLeft() {
	var _n = instance_number(obj_Animal);
	var _k = 0;
	for (var _i=0; _i<_n; _i++) {
		var _id = instance_find(obj_Animal, _i);
		if (!_id.actual_piece) {
			_k++;
		}
	}
	return _k;
}

function fnc_ControllableAnimals() {
	var _n = instance_number(obj_Animal);
	var _k = 0;
	for (var _i=0; _i<_n; _i++) {
		var _id = instance_find(obj_Animal, _i);
		if (_id.controllable || !_id.controllable && (_id.alarm[0] != -1 || _id.alarm[1] != -1)) {
			_k++;
		}
	}
	return _k;
}

#endregion