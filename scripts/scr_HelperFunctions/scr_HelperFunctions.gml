// Function to (re)initialize game variables
function fnc_InitializeGameStartVariables() {
	paused = false;
	lost = false;
	
	level = -1;
	total_score = 0;
	death_message_chosen = "";
	
	current_step = 0;	
	
	scoreboard_queried = false;
	scoreboard = [];
	scoreboard_html5 = [];
		
	http_get_id_query = noone;
	http_return_status_query = noone;
	http_return_result_query = noone;
	
	http_get_id_update = noone;
	http_return_status_update = noone;
	http_return_result_update = noone;
		
	current_scoreboard_updates = 0;
	max_scoreboard_updates = 1;
	timer_scoreboard_updates = 180;
		
	// Reset alarms (except for 0, which is center screen), since Game is persistent
	for (var _i=1; _i<=11; _i++) {
		Game.alarm[_i] = -1;
	}
	
	fullscreen_change = false;
	
	
	strikes = 0;
	game_over_played = false;
}


// Update scoreboard
function fnc_UpdateScoreboard() {
	if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
		var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/score_insert.php";
		var _hash = sha1_string_utf8(Game.option_value[? Game.option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
		var _params = "user="+Game.option_value[? Game.option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
		
		http_get_id_update = http_get(_scoreboard_url + "?" + _params);
	}
	else {
		http_get_id_update = -1;
	}
}


/// @function fnc_ChooseProb(choose_array, array_probs)
/// @description Chooses a random value from the array with specified probability distribution
/// @param choose_array The array of values to choose from. If empty array, the function will return the index instead.
/// @param array_probs The probability array of each values from the list. If empty, use uniform distribution.
/// @return The chosen element

function fnc_ChooseProb(_choose_array, _array_probs) {
	var _n_choose = array_length(_choose_array);
	var _n_probs = array_length(_array_probs);
	
	if (_n_probs == 0 && _n_choose == 0) {  // Error
		throw("Error on fnc_ChooseProb, empty arrays provided.");
	}
	else {
		if (_n_probs == 0) {	 // Set uniform distribution
			var _probs = array_create(_n_choose);
			var _k = 0;
			for (var _i=0; _i<_n_choose; _i++) {
				if (_i < _n_choose - 1) {
					_probs[_i] = 1/_n_choose;
					_k=_k+_probs[_i];
				}
				else {
					_probs[_i] = 1-_k;				
				}
			}
		}
		else {	// Use what has been given
			_probs = _array_probs;			
		}
	
		var _rnd = random(1);
		var _i = 0;
		var	_currProb = _probs[_i];
		var _cumProb = _currProb;
		while (_rnd > _cumProb) {
			_i++;
			var	_currProb = _probs[_i];
			var _cumProb = _cumProb + _currProb;
		}
		if (_n_choose == 0) {
			return _i;
		}
		else {		
			return _choose_array[_i];
		}
	}
}

/// @function fnc_ChooseProbList(choose_list, array_probs)
/// @description Chooses a random value from the list with specified probability distribution
/// @param choose_list The list of values to choose from
/// @param array_probs The probability array of each values from the list
/// @return The chosen element

function fnc_ChooseProbList(_choose_list, _array_probs) {
	var _rnd = random(1);
	var _i = 0;
	var	_currProb = _array_probs[_i];
	var _cumProb = _currProb;
	while (_rnd > _cumProb) {
		_i++;
		var	_currProb = _array_probs[_i];
		var _cumProb = _cumProb + _currProb;
	}
	return _choose_list[| _i];
}



/// @function string_to_list(string, delimiter)
/// @arg string The string to parse
/// @arg delimiter The delimiter to use
/// @return A DS List with the contents of the string separated by the chosen delimiter.
function string_to_list() {

	var _string = argument[0];
	var _delimiter = argument[1];

	var _result = ds_list_create();

	var _n = string_length(_string);

	if (_n > 0) {
		var _temp = _string;
	
		var _fin = string_pos(_delimiter, _temp);
		while (_fin > 0) {
			ds_list_add(_result, string_copy(_temp, 1, _fin-1));
			_temp = string_copy(_temp, _fin + 1, _n);
			_fin = string_pos(_delimiter, _temp);
		}
		ds_list_add(_result, string_copy(_temp, 1, string_length(_temp)));
	}

	return _result;
}


/// @function fnc_KeyToString(_key)
/// @arg _key The keycode to name
/// @return The reeadable name of the keycode

function fnc_KeyToString(_key) {
	if (_key >= 48 && _key <= 90) { 
		return chr(_key);
	}
	else {
		switch(_key) {
		    case -1: return "Unassigned";
		    case vk_backspace: return "Backspace";
		    case vk_tab: return "Tab";
		    case vk_enter: return "Enter";
		    case vk_lshift: return "Left Shift";
		    case vk_lcontrol: return "Left Ctrl";
		    case vk_lalt: return "Left Alt";
			case vk_rshift: return "Right Shift";
		    case vk_rcontrol: return "Right Ctrl";
		    case vk_ralt: return "Right Alt";
			case vk_shift: return "Shift";
		    case vk_control: return "Ctrl";
		    case vk_alt: return "Alt";
			case vk_printscreen: return "Print Screen";
		    case vk_pause: return "Pause/Break";
		    case 20: return "Caps Lock";
		    case vk_escape: return "Esc";
			case vk_space: return "Space";
		    case vk_pageup: return "Page Up";
		    case vk_pagedown: return "Page Down";
		    case vk_end: return "End";
		    case vk_home: return "Home";
		    case vk_left: return "Left Arrow";
		    case vk_up: return "Up Arrow";
		    case vk_right: return "Right Arrow";
		    case vk_down: return "Down Arrow";
		    case vk_insert: return "Insert";
		    case vk_delete: return "Delete";
			case vk_divide: return "/";
		    case vk_numpad0: return "Numpad 0";
		    case vk_numpad1: return "Numpad 1";
		    case vk_numpad2: return "Numpad 2";
		    case vk_numpad3: return "Numpad 3";
		    case vk_numpad4: return "Numpad 4";
		    case vk_numpad5: return "Numpad 5";
		    case vk_numpad6: return "Numpad 6";
		    case vk_numpad7: return "Numpad 7";
		    case vk_numpad8: return "Numpad 8";
		    case vk_numpad9: return "Numpad 9";
		    case vk_multiply: return "Numpad *";
		    case vk_add: return "Numpad +";
			case vk_decimal: return "Numpad .";
		    case vk_subtract: return "Numpad -";    
		    case vk_f1: return "F1";
		    case vk_f2: return "F2";
		    case vk_f3: return "F3";
		    case vk_f4: return "F4";
		    case vk_f5: return "F5";
		    case vk_f6: return "F6";
		    case vk_f7: return "F7";
		    case vk_f8: return "F8";
		    case vk_f9: return "F9";
		    case vk_f10: return "F10";
		    case vk_f11: return "F11";
		    case vk_f12: return "F12";
		    case 144: return "Num Lock";
		    case 145: return "Scroll Lock";
		    case ord(";"): return ";";
		    case ord("="): return "=";
		    case ord("\\"): return "\\";    
		    case ord("["): return "[";
		    case ord("]"): return "]";
			default: return "other";
		}
	}
}

/// @function fnc_BackupDrawParams
/// @description Backup the draw parameters to variables

function fnc_BackupDrawParams() {
	tmpDrawFont = draw_get_font();
	tmpDrawHAlign = draw_get_halign();
	tmpDrawVAlign = draw_get_valign();
	tmpDrawColor = draw_get_color();
	tmpDrawAlpha = draw_get_alpha();	
}

/// @function fnc_RestoreDrawParams
/// @description Restore the draw parameters from variables

function fnc_RestoreDrawParams() {
	draw_set_font(tmpDrawFont);
	draw_set_halign(tmpDrawHAlign);
	draw_set_valign(tmpDrawVAlign);
	draw_set_color(tmpDrawColor);
	draw_set_alpha(tmpDrawAlpha);	
}

function base_convert(_string_number, _old_base, _new_base) {
	var number, oldbase, newbase, out;
    number = string_upper(_string_number);
    oldbase = _old_base;
    newbase = _new_base;
    out = "";
 
    var len, tab;
    len = string_length(number);
    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    var i, num;
    for (i=0; i<len; i+=1) {
        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
    }
 
    do {
        var divide, newlen;
        divide = 0;
        newlen = 0;
        for (i=0; i<len; i+=1) {
            divide = divide * oldbase + num[i];
            if (divide >= newbase) {
                num[newlen] = divide div newbase;
                newlen += 1;
                divide = divide mod newbase;
            } else if (newlen  > 0) {
                num[newlen] = 0;
                newlen += 1;
            }
        }
        len = newlen;
        out = string_char_at(tab, divide+1) + out;
    } until (len == 0);
 
    return out;
}


function in(_element, _array) {
	if (array_length(_array) == 0) {
		return false;
	}
	else {
		var _i=0;
		var _n = array_length(_array);
		var _found = false;
		while (_i<_n && !_found) {
			if (_array[_i] == _element) {
				_found = true;
			}
			else {
				_i++;
			}
		}
	}
	return _found;
}

#region String functions

/// @function		fnc_StringToList()
/// @description	Takes a string with separators and returns a DS list of the separated string
/// @param			string - Source string to split
/// @param			separator - Optional string separator to use. Default: comma
/// @param			remove leading and trailing spaces - Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
/// @return			A DS list with the string split into the parts
function fnc_StringToList() {
	if (argument_count == 0) {		
		throw ("String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	if (argument_count >= 2) {		
		var _separator = argument[1];
		if (string_length(_separator) != 1) {
			_separator = ",";
		}
	}
	else {
		var _separator = ",";
	}
	if (argument_count >= 3) {
		var _remove_lead_trail_spaces = argument[2];
	}
	else {
		var _remove_lead_trail_spaces = false;
	}
	
		
	// Process and split
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = fnc_String_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}


function fnc_String_cleanse() {
	var _str = argument[0];
	for (var _j=0; _j<32; _j++) {
		_str = string_replace_all(_str, chr(_j), "");
	}
	return _str;
}

function fnc_String_lrtrim() {
	var _str = argument[0];
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;
}

#endregion





function Vec2(_x, _y) constructor {
	x = _x;
	y = _y;
}


function Vec3(_x, _y, _z) constructor {
	x = _x;
	y = _y;
	y = _z;
}

function display_get_gui_w() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) {
		return Game.adjusted_window_width;
	}
	else {
		return display_get_gui_width();
	}
}

function display_get_gui_h() {
	if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) {
		return Game.adjusted_window_height;
	}
	else {
		return display_get_gui_height();
	}
}

/// @function fnc_Ease 
/// @description Perform easing with an animation curve, with the equation v = v_ini + lambda * (v_target-v_ini), where lambda changes from 0 to 1 according to the animation curve.
/// @param _v_ini initial value (when t=0).
/// @param _v_target target value (when t=1).
/// @param _t value of t.
/// @param _curve animation curve asset.
/// @param _curve_channel name of the curve channel.

function fnc_Ease(_v_ini, _v_target, _t, _curve, _curve_channel) {
	var _anim_channel = animcurve_get_channel(_curve, _curve_channel);
	var _lambda = animcurve_channel_evaluate(_anim_channel, _t);	
	return _v_ini + _lambda * (_v_target - _v_ini);
}


function array_find(_array, _element) {
	var _n = array_length(_array);
	var _found = false;
	var _i=0;
	while (_i<_n && !_found) {
		if (_array[_i] == _element) {
			_found = true;
		}
		else {
			_i++;
		}
	}
	if (_found) {
		return _i;
	}
	else {
		return -1;
	}
}