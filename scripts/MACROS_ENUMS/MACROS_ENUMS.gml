// =============================================== Graphics =============================================== 
#macro VIEW view_camera[0]
#macro DISPLAY_WIDTH display_get_width()
#macro DISPLAY_HEIGHT display_get_height()
#macro ASPECT_RATIO_REAL DISPLAY_WIDTH/DISPLAY_HEIGHT
#macro ASPECT_RATIO_FORCED real(540/960)
#macro GUI_WIDTH display_get_gui_w()
#macro GUI_HEIGHT display_get_gui_h()

#macro SELECTED_ASPECT_RATIO ASPECT_RATIO_REAL

enum SCALING_TYPE {	
	RESOLUTION_ADAPTED_TO_WINDOW,				// "normal" mode - set resolution AND window size and let resolution scale to window size
	RESOLUTION_INDEPENDENT_OF_WINDOW,			// Set resolution AND set window size independently
	RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED	// Resolution and window independent, but resolution scaled to max possible size within window. If aspect ratios match, it will be the same as RESOLUTION_ADAPTED_TO_WINDOW, but if different aspect ratio, will work (i.e. square resolution on rectangular window)
}

#macro SELECTED_SCALING SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED
#macro BASE_RESOLUTION_W 540
#macro BASE_RESOLUTION_H 960
#macro BASE_WINDOW_SIZE_W 540
#macro BASE_WINDOW_SIZE_H 960

// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// 1 - Base resolution: this is the game resolution it's designed for (this will be the camera view size)
//		Notes: to achieve a perfect scaling for a 16:9 aspect ratio, one can take these values:
//		1x	1920x1080 
//		2x	960x540
//		3x	640x360
//		4x	480x270
//		5x	384x216
//		6x	320x180
//		Non perfect scaling: 1280x720
// 2 - Window resolution: OPTIONAL. Set this only if you want the WINDOW to be different size than the actual GAME RESOLUTION
// Applies to WINDOW_INDEPENDENT_OF_RESOLUTION (in this case there will be no scaling) and to RESOLUTION_SCALED_TO_WINDOW (in this case the resolution will scale)
// For RESOLUTION_SCALED_TO_WINDOW, for landscape aspect ratios, it will adjust the width automatically (so choose height wisely); for portrait aspect ratios, it will adjust the height automatically (so choose width wisely)

#macro CENTER_WINDOW true


#macro MOBILE_DEVICE ((os_type == os_android || os_type == os_ios) && os_browser == browser_not_a_browser)
#macro BROWSER (os_browser != browser_not_a_browser)


// =============================================== Technical =============================================== 

#macro TILE_SIZE 16
#macro GRID_RESOLUTION 16
#macro GAMEPAD_THRESHOLD 0.3

#macro ENABLE_SCOREBOARD false
#macro SCOREBOARD_SALT "b1yEctiv0"

enum TRANSITION {
	FADE_OUT,
	STRIPES_HORIZONTAL,
	STRIPES_VERTICAL,
	SQUARES
}

enum FACING {
	EAST,
	WEST,
	NORTH,
	SOUTH
}


#macro SCREENSHAKE_STRENGTH 10
#macro SCREENSHAKE_DURATION 30

// ICONS


#macro ICON_MUSIC_OFF	chr(base_convert("e440", 16, 10))
#macro ICON_MUSIC_ON	chr(base_convert("e405", 16, 10))
#macro ICON_SOUND_ON	chr(base_convert("e050", 16, 10))
#macro ICON_SOUND_OFF	chr(base_convert("e04f", 16, 10))
#macro ICON_RETRY		chr(base_convert("e042", 16, 10))
#macro ICON_PAUSE		chr(base_convert("e034", 16, 10))
#macro ICON_PALETTE		chr(base_convert("e40a", 16, 10))

#macro ENABLE_LIVE false


#macro ANIMAL_SCALE 0.3