/// @description 
if (ENABLE_LIVE && live_call()) return live_result;
fnc_BackupDrawParams();
var _format = "[fnt_Title][c_black][fa_center][fa_middle]";
type_formatted(GUI_WIDTH/2, 200+y, "[fa_center][fa_bottom][angle,20][scale,0.5][spr_Bear][angle,0][scale,0.8][spr_Chick][angle,-20][scale,0.5][spr_Duck]");
type_formatted(GUI_WIDTH/2, 300+y, _format+"Fa[c_green]u[c_black]n[c_green]a[c_black]-");
type_formatted(GUI_WIDTH/2, 400+y, _format+"tastic!");

var _format = "[fnt_GUI][c_black][fa_center][fa_middle]";
type_formatted(GUI_WIDTH/2, GUI_HEIGHT-250, _format+"A rotating, falling animal mayhem!");
var _format = "[fnt_GUI][c_green][fa_center][fa_middle]";
type_formatted(GUI_WIDTH/2, GUI_HEIGHT-210, _format+"2021 biyectivo - Made for KenneyJam 2021");

var _format = "[fnt_GUI][c_black][fa_center][fa_middle]";
type_formatted(GUI_WIDTH/2, 500, _format+"YOUR HIGH SCORE: "+string(Game.high_score));

var _format = "[fnt_Title][c_black][fa_center][fa_middle][scale,0.4]";
type_formatted(GUI_WIDTH/2, GUI_HEIGHT-70, _format+"PRESS [c_white]ENTER[c_black] TO START!");

var _format = "[fnt_GUI][c_black][fa_center][fa_middle][scale,0.7]";
type_formatted(GUI_WIDTH/2, GUI_HEIGHT-20, _format+"M to toggle music & sounds");

fnc_RestoreDrawParams();