/// @description 
if (!Game.paused) {
	if (ship_index == obj_LevelManager.current_controllable_ship) {
		image_alpha = 1;
	}
	else {
		image_alpha = 0.5;	
	}
	draw_self();
}