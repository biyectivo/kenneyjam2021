/// @description Global variables and data structures
#macro TYPE_FORMATTED_DEFAULT_ALPHA 1.0
#macro TYPE_FORMATTED_DEFAULT_COLOR c_black
#macro TYPE_FORMATTED_DEFAULT_HALIGN fa_left
#macro TYPE_FORMATTED_DEFAULT_VALIGN fa_top
#macro TYPE_FORMATTED_DEFAULT_XSCALE 1
#macro TYPE_FORMATTED_DEFAULT_YSCALE 1
#macro TYPE_FORMATTED_DEFAULT_FONT -1 // STRONGLY RECOMMENDED to substitute with your most used font. Use -1 for Arial 12, GMS2's default font, but beware this causes additional texture swaps
#macro TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE 0.05
#macro TYPE_FORMATTED_DEBUG_BBOX false // Game.debug
#macro TYPE_FORMATTED_DEBUG_NOTES false // Game.debug
#macro TYPE_FORMATTED_DEBUG_VERBOSE false // Game.debug
#macro TYPE_FORMATTED_AUTOFLUSH false
#macro TYPE_FORMATTED_CACHE_SIZE_FLUSH 100
#macro TYPE_FORMATTED_VERSION 0.4 // Game.debug

// Map
global.TypeFormatted_parsedElementMap = ds_map_create();

global.TypeFormatted_ParsedElement = function(_parsed_string_array, _sprite_array, _initial_subimage_array, _speed_array, _parameter_array, _bbox_width, _bbox_height, _halign, _valign, _initial_character, _typewriter_delay) constructor {
	// Keys
	parsed_string_array = _parsed_string_array;
	parameter_array = _parameter_array;
	
	sprite_array = _sprite_array;
	initial_subimage_array = _initial_subimage_array;
	speed_array = _speed_array;
	
	bbox_width = _bbox_width;
	bbox_height = _bbox_height;
	halign = _halign;
	valign = _valign;
	
	current_alarm = speed_array;
	current_subimg = _initial_subimage_array;
	
	current_character = _initial_character;
	typewriter_delay = _typewriter_delay;
	typewriter_alarm = _typewriter_delay;
	
	static setTypewriterDelay = function(_delay) {
		typewriter_delay = _delay;
		typewriter_alarm = _delay;
	}

	static restartTypewriter = function() {
		current_character = 0;
	}
	
	static endTypewriter = function() {
		current_character = totalCharacters();
		
	}
	static incrementFrame = function(_idx) {
		current_subimg[_idx] = (current_subimg[_idx]+1) % sprite_get_number(sprite_array[_idx]);
		current_alarm[_idx] = speed_array[_idx];
	}
	
	static incrementCharacter = function() {
		current_character = current_character+1;
		typewriter_alarm = typewriter_delay;
	}
	
	static totalCharacters = function() { // including "spaces" for sprites
		var _total = 0;
		var _n = array_length(parsed_string_array);
		var _m = array_length(sprite_array);
		for (var _i=0; _i<_n; _i++) {
			_total = _total + string_length(parsed_string_array[_i]);	
		}
		_total = _total + _m;
		return _total;
	}
	
	static toString = function () {
		for (var _i=0; _i<array_length(parsed_string_array); _i++) {
			show_debug_message(parsed_string_array[_i]);
		}
	}
	
	
	bbox = function(_x, _y) {
		// Determine x1/x2 initial position for bbox and alignment
		if (halign == fa_left) {
			var _x1 = _x;
			var _x2 = _x + bbox_width;							
		}
		else if (halign == fa_right) {
			var _x1 = _x - bbox_width;	
			var _x2 = _x;
		}
		else {
			var _x1 = _x - bbox_width/2;
			var _x2 = _x + bbox_width/2;
		}
						
		// Determine bbox y1 and y2
		if (valign == fa_top) {
			var _y1 = _y;
			var _y2 = _y + bbox_height;
		}
		else if (valign == fa_bottom) {
			var _y1 = _y - bbox_height;
			var _y2 = _y;							
		}
		else {
			var _y1 = _y - bbox_height/2;
			var _y2 = _y + bbox_height/2;
		}
		
		return [_x1, _y1, _x2, _y2];
	}

}

// Color and alignment constants

#region Color and alignment constants
// Color constants - HTML5 compatibility
global.tf_colors = ds_map_create();
global.tf_colors[? "c_aqua"] = $FFFF00;
global.tf_colors[? "c_black"] = $000000;
global.tf_colors[? "c_blue"] = $FF0000;
global.tf_colors[? "c_dkgray"] = $404040;
global.tf_colors[? "c_fuchsia"] = $FF00FF;
global.tf_colors[? "c_gray"] = $808080;
global.tf_colors[? "c_green"] = $008000;
global.tf_colors[? "c_lime"] = $00FF00;
global.tf_colors[? "c_ltgray"] = $C0C0C0;
global.tf_colors[? "c_maroon"] = $000080;
global.tf_colors[? "c_navy"] = $800000;
global.tf_colors[? "c_olive"] = $008080;
global.tf_colors[? "c_orange"] = $40A0FF;
global.tf_colors[? "c_purple"] = $800080;
global.tf_colors[? "c_red"] = $0000FF;
global.tf_colors[? "c_silver"] = $C0C0C0;
global.tf_colors[? "c_teal"] = $808000;
global.tf_colors[? "c_white"] = $FFFFFF;
global.tf_colors[? "c_yellow"] = $00FFFF;
global.tf_colors[? "c_dkred"] = $00008B;
global.tf_colors[? "c_brown"] = $2A2AA5;
global.tf_colors[? "c_firebrick"] = $2222B2;
global.tf_colors[? "c_crimson"] = $3C14DC;
global.tf_colors[? "c_tomato"] = $4763FF;
global.tf_colors[? "c_coral"] = $507FFF;
global.tf_colors[? "c_indianred"] = $5C5CCD;
global.tf_colors[? "c_ltcoral"] = $8080F0;
global.tf_colors[? "c_dksalmon"] = $7A96E9;
global.tf_colors[? "c_salmon"] = $7280FA;
global.tf_colors[? "c_ltsalmon"] = $7AA0FF;
global.tf_colors[? "c_orangered"] = $0045FF;
global.tf_colors[? "c_dkorange"] = $008CFF;
global.tf_colors[? "c_gold"] = $00D7FF;
global.tf_colors[? "c_dkgoldenrod"] = $0B86B8;
global.tf_colors[? "c_goldenrod"] = $20A5DA;
global.tf_colors[? "c_palegoldenrod"] = $AAE8EE;
global.tf_colors[? "c_dkkhaki"] = $6BB7BD;
global.tf_colors[? "c_khaki"] = $8CE6F0;
global.tf_colors[? "c_yellowgreen"] = $32CD9A;
global.tf_colors[? "c_dkolivegreen"] = $2F6B55;
global.tf_colors[? "c_olivedrab"] = $238E6B;
global.tf_colors[? "c_lawngreen"] = $00FC7C;
global.tf_colors[? "c_chartreuse"] = $00FF7F;
global.tf_colors[? "c_greenyellow"] = $2FFFAD;
global.tf_colors[? "c_dkgreen"] = 6400;
global.tf_colors[? "c_forestgreen"] = $228B22;
global.tf_colors[? "c_limegreen"] = $32CD32;
global.tf_colors[? "c_ltgreen"] = $90EE90;
global.tf_colors[? "c_palegreen"] = $98FB98;
global.tf_colors[? "c_dkseagreen"] = $8FBC8F;
global.tf_colors[? "c_mediumspringgreen"] = $9AFA00;
global.tf_colors[? "c_springgreen"] = $7FFF00;
global.tf_colors[? "c_seagreen"] = $578B2E;
global.tf_colors[? "c_mediumaquamarine"] = $AACD66;
global.tf_colors[? "c_mediumseagreen"] = $71B33C;
global.tf_colors[? "c_ltseagreen"] = $AAB220;
global.tf_colors[? "c_dkslategray"] = $4F4F2F;
global.tf_colors[? "c_dkcyan"] = $8B8B00;
global.tf_colors[? "c_cyan"] = $FFFF00;
global.tf_colors[? "c_ltcyan"] = $FFFFE0;
global.tf_colors[? "c_dkturquoise"] = $D1CE00;
global.tf_colors[? "c_turquoise"] = $D0E040;
global.tf_colors[? "c_mediumturquoise"] = $CCD148;
global.tf_colors[? "c_paleturquoise"] = $EEEEAF;
global.tf_colors[? "c_aquamarine"] = $D4FF7F;
global.tf_colors[? "c_powderblue"] = $E6E0B0;
global.tf_colors[? "c_cadetblue"] = $A09E5F;
global.tf_colors[? "c_steelblue"] = $B48246;
global.tf_colors[? "c_cornflowerblue"] = $ED9564;
global.tf_colors[? "c_deepskyblue"] = $FFBF00;
global.tf_colors[? "c_dodgerblue"] = $FF901E;
global.tf_colors[? "c_ltblue"] = $E6D8AD;
global.tf_colors[? "c_skyblue"] = $EBCE87;
global.tf_colors[? "c_ltskyblue"] = $FACE87;
global.tf_colors[? "c_midnightblue"] = 701919;
global.tf_colors[? "c_dkblue"] = $8B0000;
global.tf_colors[? "c_mediumblue"] = $CD0000;
global.tf_colors[? "c_pureblue"] = $FF0000;
global.tf_colors[? "c_royalblue"] = $E16941;
global.tf_colors[? "c_blueviolet"] = $E22B8A;
global.tf_colors[? "c_indigo"] = $82004B;
global.tf_colors[? "c_dkslateblue"] = $8B3D48;
global.tf_colors[? "c_slateblue"] = $CD5A6A;
global.tf_colors[? "c_mediumslateblue"] = $EE687B;
global.tf_colors[? "c_mediumpurple"] = $DB7093;
global.tf_colors[? "c_dkmagenta"] = $8B008B;
global.tf_colors[? "c_dkviolet"] = $D30094;
global.tf_colors[? "c_dkorchid"] = $CC3299;
global.tf_colors[? "c_mediumorchid"] = $D355BA;
global.tf_colors[? "c_thistle"] = $D8BFD8;
global.tf_colors[? "c_plum"] = $DDA0DD;
global.tf_colors[? "c_violet"] = $EE82EE;
global.tf_colors[? "c_magenta"] = $FF00FF;
global.tf_colors[? "c_orchid"] = $D670DA;
global.tf_colors[? "c_mediumvioletred"] = $8515C7;
global.tf_colors[? "c_palevioletred"] = $9370DB;
global.tf_colors[? "c_deeppink"] = $9314FF;
global.tf_colors[? "c_hotpink"] = $B469FF;
global.tf_colors[? "c_ltpink"] = $C1B6FF;
global.tf_colors[? "c_pink"] = $CBC0FF;
global.tf_colors[? "c_antiquewhite"] = $D7EBFA;
global.tf_colors[? "c_beige"] = $DCF5F5;
global.tf_colors[? "c_bisque"] = $C4E4FF;
global.tf_colors[? "c_blanchedalmond"] = $CDEBFF;
global.tf_colors[? "c_wheat"] = $B3DEF5;
global.tf_colors[? "c_cornsilk"] = $DCF8FF;
global.tf_colors[? "c_lemonchiffon"] = $CDFAFF;
global.tf_colors[? "c_ltgoldenrodyellow"] = $D2FAFA;
global.tf_colors[? "c_ltyellow"] = $E0FFFF;
global.tf_colors[? "c_saddlebrown"] = $13458B;
global.tf_colors[? "c_sienna"] = $2D52A0;
global.tf_colors[? "c_chocolate"] = $1E69D2;
global.tf_colors[? "c_peru"] = $3F85CD;
global.tf_colors[? "c_sandybrown"] = $60A4F4;
global.tf_colors[? "c_burlywood"] = $87B8DE;
global.tf_colors[? "c_tan"] = $8CB4D2;
global.tf_colors[? "c_rosybrown"] = $8F8FBC;
global.tf_colors[? "c_moccasin"] = $B5E4FF;
global.tf_colors[? "c_navajowhite"] = $ADDEFF;
global.tf_colors[? "c_peachpuff"] = $B9DAFF;
global.tf_colors[? "c_mistyrose"] = $E1E4FF;
global.tf_colors[? "c_lavenderblush"] = $F5F0FF;
global.tf_colors[? "c_linen"] = $E6F0FA;
global.tf_colors[? "c_oldlace"] = $E6F5FD;
global.tf_colors[? "c_papayawhip"] = $D5EFFF;
global.tf_colors[? "c_seashell"] = $EEF5FF;
global.tf_colors[? "c_mintcream"] = $FAFFF5;
global.tf_colors[? "c_slategray"] = $908070;
global.tf_colors[? "c_ltslategray"] = $998877;
global.tf_colors[? "c_ltsteelblue"] = $DEC4B0;
global.tf_colors[? "c_lavender"] = $FAE6E6;
global.tf_colors[? "c_floralwhite"] = $F0FAFF;
global.tf_colors[? "c_aliceblue"] = $FFF8F0;
global.tf_colors[? "c_ghostwhite"] = $FFF8F8;
global.tf_colors[? "c_honeydew"] = $F0FFF0;
global.tf_colors[? "c_ivory"] = $F0FFFF;
global.tf_colors[? "c_azure"] = $FFFFF0;
global.tf_colors[? "c_snow"] = $FAFAFF;
global.tf_colors[? "c_gainsboro"] = $DCDCDC;
global.tf_colors[? "c_whitesmoke"] = $F5F5F5;
	
// Alignment constants - HTML5 compatibility
global.tf_valignments = ds_map_create();
global.tf_valignments[? "fa_top"] = 0;
global.tf_valignments[? "fa_middle"] = 1;
global.tf_valignments[? "fa_bottom"] = 2;
	
global.tf_halignments = ds_map_create();
global.tf_halignments[? "fa_left"] = 0;
global.tf_halignments[? "fa_center"] = 1;
global.tf_halignments[? "fa_right"] = 2;

#endregion


show_debug_message("TypeFormatted version "+string(TYPE_FORMATTED_VERSION)+" by biyectivo");