/// @description 
if (!Game.paused) {
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Strike, 1, false);
	}
	instance_destroy(other);
	obj_LevelManager.increase_level_blood = true;
}