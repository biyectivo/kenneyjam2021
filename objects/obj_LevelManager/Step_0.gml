/// @description 
if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {
	
	if (!Game.lost) {
		if (fnc_AnimalsLeft() == 0) {
			Game.level++;
			if (Game.level >0 && Game.option_value[? "Sounds"]) {
				audio_play_sound(snd_LevelUp, 1, false);
			}
		
			var _index = min(array_length(level_cols)-1, Game.level);
			board_columns = level_cols[_index];
			board_rows = level_rows[_index];
	
			elegible_animals = [];
			var _n = min(board_columns*board_rows, array_length(total_animals));
		
			for (var _i=0; _i<_n; _i++) {
				var _k = fnc_ChooseProb(total_animals, []);
				array_push(elegible_animals, _k);
			}
		
			total_width = (board_columns*base_animal_width+(board_columns-1)*base_spacing_width)*ANIMAL_SCALE;
			total_height = (board_rows*base_animal_height+(board_rows-1)*base_spacing_height)*ANIMAL_SCALE;
	
			board_x_start = (room_width - total_width)/2 + base_animal_width*ANIMAL_SCALE/2;
			board_y_start = room_height - total_height-150;
	
			with (obj_Animal) {
				instance_destroy();
			}
			with (obj_Board) {
				instance_destroy();
			}
			with (obj_Points) {
				instance_destroy();
			}
			with (cls_Ship) {
				instance_destroy();
			}
			with (cls_Bullet) {
				instance_destroy();
			}
	
		
			// Generate board
			var _id = instance_create_layer(board_x_start - base_animal_width*ANIMAL_SCALE, board_y_start - base_animal_height*ANIMAL_SCALE*2, "lyr_Board", obj_Board);
			with (_id) {
				board_width = other.total_width + other.base_animal_width*ANIMAL_SCALE;
				board_height = other.total_height + 3*other.base_animal_height*ANIMAL_SCALE;
			}
		
			chosen_animals = [];
			// Generate animal targets
			for (var _row=0; _row<board_rows;_row++) {
				for (var _col=0; _col<board_columns; _col++) {
					var _id = instance_create_layer(board_x_start + _col*(base_animal_width + base_spacing_width)*ANIMAL_SCALE, board_y_start + _row*(base_animal_height + base_spacing_height)*ANIMAL_SCALE, "lyr_Board_Targets", obj_Animal);
					with (_id) {
						sprite_index = fnc_ChooseProb(other.elegible_animals, []);
						image_angle = choose(0, 90, 180, 270);
						controllable = false;
						actual_piece = false;
					
						if (array_find(other.chosen_animals, sprite_index) == -1) {
							array_push(other.chosen_animals, sprite_index);						
						}
					}
				}
			}
		
			// Generate rotator ship
			var _id = instance_create_layer(board_x_start + total_width + 50, animal_y_start + 100, "lyr_Instances", obj_Ship_Rotator);
			with (_id) {
				ship_index = 0;
			}
		
			// Generate transformer ship
			var _id = instance_create_layer(board_x_start + total_width + 50, animal_y_start + 100 + (board_y_start-120 - animal_y_start-100)/2, "lyr_Instances", obj_Ship_Transformer);
			with (_id) {
				ship_index = 1;
			}
		
			// Generate saver ship
			var _id = instance_create_layer(board_x_start + total_width + 50, board_y_start - 120, "lyr_Instances", obj_Ship_Saver);
			with (_id) {
				ship_index = 2;
			}
		
		}
	
		if (instance_number(obj_Board) > 0 && keyboard_check_pressed(vk_space)) {
			current_controllable_ship = (current_controllable_ship+1) % instance_number(cls_Ship);
		}
	
		// Spawn animal
		if (instance_number(obj_Board) > 0 && fnc_ControllableAnimals() == 0) {			
			var _row = 0;
			var _col = irandom_range(0, board_columns-1);
			var _id = instance_create_layer(board_x_start + _col*(base_animal_width + base_spacing_width)*ANIMAL_SCALE, animal_y_start, "lyr_Instances", obj_Animal);
			with (_id) {
				sprite_index = fnc_ChooseProb(other.chosen_animals, []);
				column =_col;
				controllable = true;
				actual_piece = true;
			}	
		}
	
	
		// Blood
		if (increase_level_blood) {
			if (alarm[0] == -1) {
				alarm[0] = 45;
				baseline_blood_pct = level_blood_pct;
			}
			var _t = 1-alarm[0]/45;
			level_blood_pct = fnc_Ease(baseline_blood_pct, baseline_blood_pct+0.34, _t, CSSTransitions, "ease-back");		
		}
		
		if (Game.strikes == 3) {
			Game.lost = true;
			Game.paused = true;
		}
	}
	else {
		instance_deactivate_all(true);
		instance_activate_object(Game);
	}
}