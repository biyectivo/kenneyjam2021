/// @description 
if (!Game.paused) {
	if (level_blood_pct > 0) {
		fnc_BackupDrawParams();
		var _h = room_height * level_blood_pct;
		draw_set_alpha(0.2 + 0.2*Game.strikes);
		draw_rectangle_color(0, room_height-_h, room_width, room_height, c_red, c_red, c_red, c_red, false);
		fnc_RestoreDrawParams();
	}
}