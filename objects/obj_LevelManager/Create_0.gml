/// @description 
level_blood_pct = 0;
baseline_blood_pct = 0;
increase_level_blood = false;
current_controllable_ship = 0;


base_animal_width = 162;
base_animal_height = 136;
base_spacing_width = 30;
base_spacing_height = 30;
animal_y_start = 100;	
	
	
total_animals = [spr_Bear, spr_Chick, spr_Chicken, spr_Cow, spr_Crocodile, spr_Duck, spr_Giraffe, spr_Gorilla, spr_Hippo, spr_Horse, spr_Narwhal, spr_Penguin, spr_Pig, spr_Rhino, spr_Whale, spr_Zebra];

level_rows = [2, 2, 3, 3, 4, 4, 5, 6]; 
level_cols = [2, 3, 2, 3, 4, 5, 5, 6];

audio_stop_all();
if (Game.option_value[? "Music"]) {
	audio_play_sound(snd_Music_Main, 100, true);
}