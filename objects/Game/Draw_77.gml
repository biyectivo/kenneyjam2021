/// @description Draw Game

// Draw app surface
if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW) {
	if (window_get_fullscreen()) {
		var _offsetx = (DISPLAY_WIDTH - adjusted_resolution_width)/2;
		var _offsety = (DISPLAY_HEIGHT - adjusted_resolution_height)/2;
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
	}
	else {
		var _offsetx = (adjusted_window_width - adjusted_resolution_width)/2;
		var _offsety = (adjusted_window_height - adjusted_resolution_height)/2;	
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
	}
	
	gpu_set_blendenable(false);
	draw_surface_ext(application_surface, _offsetx, _offsety, 1, 1, 0, c_white, 1);	
	gpu_set_blendenable(true);
	
	//display_set_gui_maximize(_factorw, _factorh, 0, 0);
	display_set_gui_size(adjusted_window_width, adjusted_window_height);
}
else if (SELECTED_SCALING == SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW_MAXIMIZED) {
	if (window_get_fullscreen()) {
		var _factorw = DISPLAY_WIDTH / adjusted_resolution_width;
		var _factorh = DISPLAY_HEIGHT / adjusted_resolution_height;		
		var _scale = min(_factorw, _factorh);
		var _offsetx = (DISPLAY_WIDTH - adjusted_resolution_width*_scale)/2;
		var _offsety = (DISPLAY_HEIGHT - adjusted_resolution_height*_scale)/2;		
	}
	else {
		var _factorw = adjusted_window_width / adjusted_resolution_width;
		var _factorh = adjusted_window_height / adjusted_resolution_height;
		var _scale = min(_factorw, _factorh);
		var _offsetx = (adjusted_window_width - adjusted_resolution_width*_scale)/2;
		var _offsety = (adjusted_window_height - adjusted_resolution_height*_scale)/2;		
	}
	
	gpu_set_blendenable(false);
	draw_surface_ext(application_surface, _offsetx, _offsety, _scale, _scale, 0, c_white, 1);		
	gpu_set_blendenable(true);
	
	// GUI layer - workaround!!! TODO: fix this
	display_set_gui_maximize();
	
}
else {
	display_set_gui_size(adjusted_window_width, adjusted_window_height);	
}


if (CENTER_WINDOW && alarm[0] == -1) {
	alarm[0] = 2;
}