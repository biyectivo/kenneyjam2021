/// @description Debug functions

if (Game.debug) {
	// Room restart
	if (keyboard_check_pressed(vk_f2)) {
		room_restart();	
	}

	// Game restart
	if (keyboard_check_pressed(vk_f3)) {
		game_restart();
	}
	
	if (keyboard_check_pressed(ord("D"))) {
		Game.level++;
	}
	
	if (keyboard_check_pressed(ord("A"))) {
		Game.level = max(0, Game.level-1);
	}	
}

// Toggle debug mode
if (keyboard_check_pressed(vk_f1)) {
	debug = !debug;
}

