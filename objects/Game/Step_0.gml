if (ENABLE_LIVE && live_call()) return live_result;

if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}


// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
	}
}


if (keyboard_check_pressed(ord("M"))) {
	Game.option_value[?"Sounds"] = !Game.option_value[?"Sounds"];
	Game.option_value[?"Music"] = !Game.option_value[?"Music"];
	if (Game.option_value[?"Music"]) {
		audio_resume_all();	
	}
	else {
		audio_pause_all();	
	}
}