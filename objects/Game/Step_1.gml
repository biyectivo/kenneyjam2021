//  Handle fullscreen change
if (keyboard_check(vk_lalt) && keyboard_check_pressed(vk_enter)) {
	fnc_SetGraphics();
}

// Fullscreen has changed (either via ALT+TAB or F10 on HTML5 or via menu)
if (fullscreen_change) {	
	window_set_fullscreen(option_value[? "Fullscreen"]);
	fullscreen_change = false;	
	// Update graphics
	fnc_SetGraphics();
	
}
