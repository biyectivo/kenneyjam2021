// Enable views and set up graphics
view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_master_gain(option_value[? "Volume"]);	


if (room == room_Game_1) {
		
	// Define pathfinding grid
	/*	
	grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
	// Add collision tiles to grid			
	for (var _col = 0; _col < room_width/GRID_RESOLUTION; _col++) {
		for (var _row = 0; _row < room_height/GRID_RESOLUTION; _row++) {				
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) != 0) {
				mp_grid_add_cell(grid, _col, _row);
			}
		}
	}
	
	*/
	// Initialize particle system and emitters
		
	particle_system = part_system_create_layer("lyr_Particles", true);
	particle_emitter_fire = part_emitter_create(particle_system);
		
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Play music	
	if (option_value[? "Music"]) {
		//music_sound_id = audio_play_sound(snd_Music, 1, true);
	}
	
	// Walls with occluders
	/*
	var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision"));
	var _cols = tilemap_get_width(_tilemap);
	var _rows = tilemap_get_height(_tilemap);
	for (var _row=0; _row<_rows; _row++) {
		for (var _col=0; _col<_cols; _col++) {
			var _data = tilemap_get(_tilemap, _col, _row);
			var _ind = tile_get_index(_data);
			var _x = _col * TILE_SIZE + TILE_SIZE/2;
			var _y = _row * TILE_SIZE + TILE_SIZE/2;
			if (_ind == 1) { // Create wall object with occluder
				var _id = instance_create_layer(_x, _y, layer_get_id("lyr_Occluders"), obj_Wall);				
			}
		}
	}
	*/
	
	
	// Scribble - add fonts to included files first
	//scribble_font_add("fnt_Title");
	
	
	// High score
	var _fid = file_text_open_read("high_score.dat");
	var _line = file_text_readln(_fid);
	var _split = fnc_StringToList(_line, " ");
	var _score = real(_split[|0]);
	var _level = real(_split[|1]);
	var _hash = string_replace(_split[|2], "\n", "");
	var _str = string(_score)+" "+string(_level)+" "+SCOREBOARD_SALT;
	
	if (sha1_string_utf8(_str) == _hash) {
		show_debug_message("High score retrieved "+_split[|0]+" "+_split[|1]);
		Game.high_score = _score;
		Game.high_level = _level;
		file_text_close(_fid);
	}
	else {
		show_debug_message("High score corrupted! "+string(_score)+" "+string(_level)+" does not compute: "+sha1_string_utf8(_str)+" vs "+_hash);
		file_text_close(_fid);
		file_delete("high_score.dat");
		var _fid = file_text_open_write("high_score.dat");
		file_text_write_string(_fid, string(Game.total_score)+" "+string(Game.level+1)+" "+sha1_string_utf8(string(Game.total_score)+" "+string(Game.level+1)+" "+SCOREBOARD_SALT));
		file_text_close(_fid);
	}
	
}
