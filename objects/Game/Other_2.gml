// Mouse cursor
//window_set_cursor(cr_none);
//cursor_sprite = spr_Cursor;

// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
if (SELECTED_SCALING != SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW) {
	application_surface_draw_enable(false);
}

// High score
if (!file_exists("high_score.dat")) {	
	var _fid = file_text_open_write("high_score.dat");
	file_text_write_string(_fid, string(Game.total_score)+" "+string(Game.level+1)+" "+sha1_string_utf8(string(Game.total_score)+" "+string(Game.level+1)+" "+SCOREBOARD_SALT));
	file_text_close(_fid);
	show_debug_message("High score file created "+string(Game.total_score)+" "+string(Game.level+1));
}
else {
	
	var _fid = file_text_open_read("high_score.dat");
	var _line = file_text_readln(_fid);
	var _split = fnc_StringToList(_line, " ");
	var _score = real(_split[|0]);
	var _level = real(_split[|1]);
	var _hash = string_replace(_split[|2], "\n", "");
	var _str = string(_score)+" "+string(_level)+" "+SCOREBOARD_SALT;
	
	if (sha1_string_utf8(_str) == _hash) {
		show_debug_message("High score retrieved "+_split[|0]+" "+_split[|1]);
		Game.high_score = _score;
		Game.high_level = _level;
		file_text_close(_fid);
	}
	else {
		show_debug_message("High score corrupted! "+string(_score)+" "+string(_level)+" does not compute: "+sha1_string_utf8(_str)+" vs "+_hash);
		file_text_close(_fid);
		file_delete("high_score.dat");
		var _fid = file_text_open_write("high_score.dat");
		file_text_write_string(_fid, string(Game.total_score)+" "+string(Game.level+1)+" "+sha1_string_utf8(string(Game.total_score)+" "+string(Game.level+1)+" "+SCOREBOARD_SALT));
		file_text_close(_fid);
	}
}

// Go to next room
room_goto(room_UI_Title);
//room_goto(room_Game_1);
