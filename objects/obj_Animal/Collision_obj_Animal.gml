/// @description 
if (!Game.paused) {
	if (!actual_piece && other.actual_piece && sprite_index == other.sprite_index && image_angle == other.image_angle && abs(y - other.y) < 16) {
		actual_piece = true;
		
		// Score
		Game.total_score = Game.total_score + 100;
		var _id = instance_create_layer(x, y, "lyr_Points", obj_Points);
		with (_id) {		
			point_value = 100;
		}
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_AnimalOK, 1, false);
		}
		
		instance_destroy(other);
	}
}