/// @description 
if (!Game.paused) {
	if (controllable) {
		
		if (keyboard_check_pressed(vk_left)) {						
			baseline_x = x;
			column = clamp(column-1, 0, obj_LevelManager.board_columns-1);
			if (alarm[0] == -1) {
				alarm[0] = 10;				
			}
		}
		
		if (keyboard_check_pressed(vk_right)) {
			baseline_x = x;
			column = clamp(column+1, 0, obj_LevelManager.board_columns-1);			
			if (alarm[0] == -1) {
				alarm[0] = 10;				
			}
		}
		
		if (float) {
			fall_speed = max(-3, -2 - (Game.level*0.25));
		}
		else {			
			if (keyboard_check(vk_down)) {
				fall_speed = 5;
			}
			else {
				fall_speed = min(4, 0.5 + (Game.level*0.25));
			}
		}
		
	}
	
	if (actual_piece) {
		if (alarm[0] != -1) {			
			controllable = false;
			var _target = obj_LevelManager.board_x_start + (column * obj_LevelManager.base_animal_width + column * obj_LevelManager.base_spacing_width)*ANIMAL_SCALE;
			var _t = 1-alarm[0]/10;
			x = fnc_Ease(baseline_x, _target, _t, CSSTransitions, "expo");			
			if (alarm[0] == 0) {
				controllable = true;
			}
		}
		
		if (alarm[1] != -1) {
			controllable = false;
			var _target = baseline_angle + 90;
			var _t = 1-alarm[1]/10;
			image_angle = fnc_Ease(baseline_angle, _target, _t, CSSTransitions, "expo");
			if (alarm[1] == 0) {
				controllable = true;
			}
		}
	
		// Angle correction post animation (to prevent turn in the other direction)
		if (image_angle == 360) image_angle = 0;		
	}
	
	if (controllable || (!controllable && (alarm[0] != -1 || alarm[1] != -1))) {
		y = y + fall_speed;
		
		// Saved, incur penalty
		if (y < obj_LevelManager.animal_y_start) {
			Game.total_score = max(0, Game.total_score - 10);
			var _id = instance_create_layer(x, y+40, "lyr_Points", obj_Points);
			with (_id) {
				point_value = -10;
			}
			instance_destroy();
		}
	}
}