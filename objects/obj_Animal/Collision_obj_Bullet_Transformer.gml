/// @description 
if (!Game.paused) {	
	var _old_sprite_index = sprite_index;
	while (_old_sprite_index == sprite_index) {
		sprite_index = fnc_ChooseProb(obj_LevelManager.chosen_animals, []);
	}
	Game.total_score = max(0, Game.total_score - 5);
	var _id = instance_create_layer(x, y+30, "lyr_Points", obj_Points);
	with (_id) {
		point_value = -5;
	}
	instance_destroy(other);
}