/// @description 
if (!Game.paused) {
	fnc_BackupDrawParams();
	var _alpha = actual_piece ? 1 : 0.3;
	image_alpha = _alpha;
	draw_sprite_ext(sprite_index, image_index, x, y, ANIMAL_SCALE, ANIMAL_SCALE, image_angle, image_blend, image_alpha);
	fnc_RestoreDrawParams();
}