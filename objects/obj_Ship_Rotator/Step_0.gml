/// @description 
if (!Game.paused) {
	y = ystart - 30*dcos(4*alarm[0]);
	if (ship_index == obj_LevelManager.current_controllable_ship && keyboard_check_pressed(vk_up)) {
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Shoot, 1, false);
		}
		var _id = instance_create_layer(x-10, y, "lyr_Instances", obj_Bullet_Rotator);			
		with (_id) {
			image_index = other.image_index;
		}
	}
}