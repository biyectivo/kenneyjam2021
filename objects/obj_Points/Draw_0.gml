/// @description 
if (!Game.paused) {
	fnc_BackupDrawParams();
	var _symbol = (point_value >= 0 ? "+" : "");
	var _color = (point_value >= 0 ? "[c_white]" : "[c_red]");
	var _format = "[fnt_GUI]"+_color+"[fa_left][fa_middle][alpha,"+string(alarm[0]/45)+"]";
	type_formatted(x+1, y+1, _format+_symbol+string(point_value));
	var _format = "[fnt_GUI]"+_color+"[fa_left][fa_middle][alpha,"+string(alarm[0]/45)+"]";
	type_formatted(x, y, _format+_symbol+string(point_value));
	fnc_RestoreDrawParams();
}